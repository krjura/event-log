package org.krjura.events.log.server.support;

import io.vertx.junit5.VertxTestContext;
import org.krjura.events.log.server.repository.PostgresConnector;

import java.util.concurrent.TimeUnit;

public class DbCleaner {

    private final PostgresConnector connector;

    public DbCleaner(final PostgresConnector connector) {
        this.connector = connector;
    }

    public void cleanAll() throws InterruptedException {
        var eventRecordContext = new VertxTestContext();
        connector.getClient().preparedQuery("DELETE from event_record", handler -> {
            if(handler.succeeded()) {
                eventRecordContext.completeNow();
            } else {
                eventRecordContext.failNow(handler.cause());
            }
        });
        eventRecordContext.awaitCompletion(5, TimeUnit.SECONDS);

        var eventsContext = new VertxTestContext();
        connector.getClient().preparedQuery("DELETE from event", handler -> {
            if(handler.succeeded()) {
                eventsContext.completeNow();
            } else {
                eventsContext.failNow(handler.cause());
            }
        });
        eventsContext.awaitCompletion(5, TimeUnit.SECONDS);

        var userContext = new VertxTestContext();
        connector.getClient().preparedQuery("DELETE from app_user", handler -> {
            if(handler.succeeded()) {
                userContext.completeNow();
            } else {
                userContext.failNow(handler.cause());
            }
        });
        userContext.awaitCompletion(5, TimeUnit.SECONDS);
    }

}
