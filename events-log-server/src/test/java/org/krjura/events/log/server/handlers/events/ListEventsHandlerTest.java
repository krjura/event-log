package org.krjura.events.log.server.handlers.events;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Test;
import org.krjura.events.log.server.model.AppUserModel;
import org.krjura.events.log.server.model.EventModel;
import org.krjura.events.log.server.support.BaseTest;
import org.krjura.events.log.server.web.events.ListEventResponse;

import java.time.ZonedDateTime;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.events.log.server.utils.JsonUtils.mapToObject;

public class ListEventsHandlerTest extends BaseTest {

    private AppUserModel defaultUser;

    @Test
    public void mustReturnEmptyListWhenNoEvents() throws Throwable {
        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        client
                .get(8080, "127.0.0.1", "/events/1")
                .send(requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(200);

                    var events = mapToObject(response.bodyAsString(), ListEventResponse.class);
                    assertThat(events).isNotNull();
                    assertThat(events.getEvents()).hasSize(0);

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    @Test
    public void mustReturnListOfEvents() throws Throwable {
        createDefaultUser();
        createDefaultEvents();

        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        client
                .get(8080, "127.0.0.1", "/events/" + defaultUser.getUserId())
                .send(requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(200);

                    var events = mapToObject(response.bodyAsString(), ListEventResponse.class);
                    assertThat(events).isNotNull();
                    assertThat(events.getEvents()).hasSize(2);

                    assertThat(events.getEvents().get(0).getId()).isNotNull();
                    assertThat(events.getEvents().get(0).getUserId()).isEqualTo(defaultUser.getUserId());
                    assertThat(events.getEvents().get(0).getSubject()).isEqualTo("event 1");
                    assertThat(events.getEvents().get(0).getDescription()).isEqualTo("event 1 description");

                    assertThat(events.getEvents().get(1).getId()).isNotNull();
                    assertThat(events.getEvents().get(1).getUserId()).isEqualTo(defaultUser.getUserId());
                    assertThat(events.getEvents().get(1).getSubject()).isEqualTo("event 2");
                    assertThat(events.getEvents().get(1).getDescription()).isEqualTo("event 2 description");

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    private void createDefaultEvents() throws Throwable {
        var now = ZonedDateTime.now();
        var event1 = server()
                .getEventsRepo()
                .save(EventModel.of(defaultUser.getUserId(), "event 1", "event 1 description", now, now))
                .get();

        var event2 = server()
                .getEventsRepo()
                .save(EventModel.of(defaultUser.getUserId(), "event 2", "event 2 description", now, now))
                .get();

        failOnError(event1, event2);
    }

    private void createDefaultUser() throws Throwable {
        var handler = server().getUserRep().save("demo@example.com").get();
        handler.onSuccess(appUserModel -> defaultUser = appUserModel);

        failOnError(handler);
    }
}
