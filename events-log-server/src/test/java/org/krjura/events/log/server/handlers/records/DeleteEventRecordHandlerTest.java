package org.krjura.events.log.server.handlers.records;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Test;
import org.krjura.events.log.server.model.AppUserModel;
import org.krjura.events.log.server.model.EventModel;
import org.krjura.events.log.server.model.EventRecord;
import org.krjura.events.log.server.support.BaseTest;

import java.time.ZonedDateTime;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

public class DeleteEventRecordHandlerTest extends BaseTest {

    private AppUserModel defaultUser;

    private EventModel defaultEvent;

    private EventRecord defaultEventRecord;

    @Test
    public void mustDeleteEventWhenNoData() throws Throwable {
        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        client
                .delete(8080, "127.0.0.1", "/event-records/1/1/1")
                .send(requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(204);

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    @Test
    public void mustDeleteEventRecord() throws Throwable {
        createDefaultUser();
        createDefaultEvent();
        createDefaultEventRecord();

        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        var url = format(
                "/event-records/%s/%s/%s",
                defaultUser.getUserId(), defaultEvent.getId(), defaultEventRecord.getId()
        );

        client
                .delete(8080, "127.0.0.1", url)
                .send(requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(204);

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();

        verifyNoEventRecordsExists();
    }

    private void verifyNoEventRecordsExists() throws Throwable {
        var handler = server().getEventRecordRepo().list(defaultUser.getUserId(), defaultEvent.getId(), 0, 50).get();

        failOnError(handler);

        assertThat(handler.isError()).isFalse();
        assertThat(handler.getData()).hasSize(0);
    }

    private void createDefaultEventRecord() throws Throwable {
        var now = ZonedDateTime.now();
        var event = EventRecord.of(defaultUser.getUserId(), defaultEvent.getId(), "event record", now, now);

        var handler = server().getEventRecordRepo().save(event).get();
        handler.onSuccess(newEvent -> defaultEventRecord = newEvent);

        failOnError(handler);
    }

    private void createDefaultEvent() throws Throwable {
        var now = ZonedDateTime.now();
        var event = EventModel.of(defaultUser.getUserId(), "demo subject", "demo description", now, now);

        var handler = server().getEventsRepo().save(event).get();
        handler.onSuccess(newEvent -> defaultEvent = newEvent);

        failOnError(handler);
    }

    private void createDefaultUser() throws Throwable {
        var handler = server().getUserRep().save("demo@example.com").get();
        handler.onSuccess(appUserModel -> defaultUser = appUserModel);

        failOnError(handler);
    }

}