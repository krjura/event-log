package org.krjura.events.log.server.support;

import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.krjura.events.log.server.EventsLogServer;
import org.krjura.events.log.server.utils.Either;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class BaseTest {

    private EventsLogServer server;

    @BeforeEach
    public void init() throws InterruptedException {
        ensureServerIsStarted();

        new DbCleaner(server.getConnector()).cleanAll();
    }

    private void ensureServerIsStarted() throws InterruptedException {
        var startTestContext = new VertxTestContext();

        var serverInit = new EventsLogServer();
        serverInit.start(startTestContext.completing());

        assertThat(startTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();

        this.server = serverInit;
    }

    @AfterEach
    public void destroy() {
        this.server.close();
    }

    public EventsLogServer server() {
        return server;
    }

    protected void failOnError(Either<?>... vals) throws Throwable {
        for(Either<?> val : vals) {
            if(val.isError()) {
                throw val.getError();
            }
        }
    }
}
