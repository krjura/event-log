package org.krjura.events.log.server.handlers.events;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Test;
import org.krjura.events.log.server.model.AppUserModel;
import org.krjura.events.log.server.model.EventModel;
import org.krjura.events.log.server.support.BaseTest;
import org.krjura.events.log.server.utils.JsonUtils;
import org.krjura.events.log.server.web.errors.ErrorResponse;

import java.time.ZonedDateTime;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class DeleteEventHandlerTest extends BaseTest {

    private AppUserModel defaultUser;
    private EventModel defaultEvent;

    @Test
    public void mustFailWhenParamsNotIntegers() throws Throwable {
        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        client
                .delete(8080, "127.0.0.1", "/events/ab/ac")
                .send(requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(400);

                    var error = JsonUtils.mapToObject(response.bodyAsString(), ErrorResponse.class);
                    assertThat(error).isNotNull();
                    assertThat(error.getDetails()).hasSize(1);
                    assertThat(error.getDetails().get(0).getReason()).isEqualTo("events.web.param.notFound");
                    assertThat(error.getDetails().get(0).getAttributeName()).isEqualTo("userId");
                    assertThat(error.getDetails().get(0).getAttributeValues()).hasSize(1);
                    assertThat(error.getDetails().get(0).getAttributeValues().get(0)).isEqualTo("null");

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    @Test
    public void mustDeleteEventWhenNoData() throws Throwable {

        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        client
                .delete(8080, "127.0.0.1", "/events/1/1")
                .send(requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(204);

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    @Test
    public void mustDeleteEvent() throws Throwable {
        createDefaultUser();
        createDefaultEvents();

        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        client
                .delete(8080, "127.0.0.1", "/events/" + defaultUser.getUserId() + "/" + defaultEvent.getId())
                .send(requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(204);

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    private void createDefaultEvents() throws Throwable {
        var now = ZonedDateTime.now();
        var event = server()
                .getEventsRepo()
                .save(EventModel.of(defaultUser.getUserId(), "event 1", "event 1 description", now, now))
                .get();

        failOnError(event);


        this.defaultEvent = event.getData();
    }

    private void createDefaultUser() throws Throwable {
        var handler = server().getUserRep().save("demo@example.com").get();
        handler.onSuccess(appUserModel -> defaultUser = appUserModel);

        failOnError(handler);
    }
}