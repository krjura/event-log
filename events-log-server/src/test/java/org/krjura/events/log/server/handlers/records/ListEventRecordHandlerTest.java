package org.krjura.events.log.server.handlers.records;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Test;
import org.krjura.events.log.server.model.AppUserModel;
import org.krjura.events.log.server.model.EventModel;
import org.krjura.events.log.server.model.EventRecord;
import org.krjura.events.log.server.support.BaseTest;
import org.krjura.events.log.server.web.events.ListEventResponse;
import org.krjura.events.log.server.web.records.ListEventRecordResponse;

import java.time.ZonedDateTime;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.krjura.events.log.server.utils.JsonUtils.mapToObject;

public class ListEventRecordHandlerTest extends BaseTest {

    private AppUserModel defaultUser;

    private EventModel defaultEvent;

    @Test
    public void mustListEventRecordsWhenNoData() throws Throwable {
        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        var url = format("/event-records/%s/%s", 1, 1);

        client
                .get(8080, "127.0.0.1", url)
                .send(requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(200);

                    var records = mapToObject(response.bodyAsString(), ListEventRecordResponse.class);
                    assertThat(records).isNotNull();
                    assertThat(records.getRecords()).hasSize(0);

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    @Test
    public void mustListEventRecords() throws Throwable {
        createDefaultUser();
        createDefaultEvent();
        createDefaultEventRecord();

        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        var url = format("/event-records/%s/%s", defaultUser.getUserId(), defaultEvent.getId());

        client
                .get(8080, "127.0.0.1", url)
                .send(requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(200);

                    var records = mapToObject(response.bodyAsString(), ListEventRecordResponse.class);
                    assertThat(records).isNotNull();
                    assertThat(records.getRecords()).hasSize(2);

                    assertThat(records.getRecords().get(0).getId()).isNotNull();
                    assertThat(records.getRecords().get(0).getUserId()).isEqualTo(defaultUser.getUserId());
                    assertThat(records.getRecords().get(0).getEventId()).isEqualTo(defaultEvent.getId());
                    assertThat(records.getRecords().get(0).getDescription()).isEqualTo("event record 1");

                    assertThat(records.getRecords().get(1).getId()).isNotNull();
                    assertThat(records.getRecords().get(1).getUserId()).isEqualTo(defaultUser.getUserId());
                    assertThat(records.getRecords().get(1).getEventId()).isEqualTo(defaultEvent.getId());
                    assertThat(records.getRecords().get(1).getDescription()).isEqualTo("event record 2");

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    private void createDefaultEventRecord() throws Throwable {
        var now = ZonedDateTime.now();
        var event1 = EventRecord.of(defaultUser.getUserId(), defaultEvent.getId(), "event record 1", now, now);
        var event2 = EventRecord.of(defaultUser.getUserId(), defaultEvent.getId(), "event record 2", now, now);

        var handler1 = server().getEventRecordRepo().save(event1).get();
        var handler2 = server().getEventRecordRepo().save(event2).get();

        failOnError(handler1, handler2);
    }

    private void createDefaultEvent() throws Throwable {
        var now = ZonedDateTime.now();
        var event = EventModel.of(defaultUser.getUserId(), "demo subject", "demo description", now, now);

        var handler = server().getEventsRepo().save(event).get();
        handler.onSuccess(newEvent -> defaultEvent = newEvent);

        failOnError(handler);
    }

    private void createDefaultUser() throws Throwable {
        var handler = server().getUserRep().save("demo@example.com").get();
        handler.onSuccess(appUserModel -> defaultUser = appUserModel);

        failOnError(handler);
    }
}