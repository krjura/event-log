package org.krjura.events.log.server.handlers.records;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Test;
import org.krjura.events.log.server.model.AppUserModel;
import org.krjura.events.log.server.model.EventModel;
import org.krjura.events.log.server.support.BaseTest;
import org.krjura.events.log.server.utils.JsonUtils;
import org.krjura.events.log.server.web.errors.ErrorResponse;
import org.krjura.events.log.server.web.records.CreateEventRecordRequest;
import org.krjura.events.log.server.web.records.EventRecordResponse;

import java.time.ZonedDateTime;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateEventRecordHandlerTest extends BaseTest {

    private AppUserModel defaultUser;

    private EventModel defaultEvent;

    @Test
    public void cannotBeSavedWhenNoEvent() throws Throwable {
        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        var request = new CreateEventRecordRequest(1, 1, "event record");
        client
                .post(8080, "127.0.0.1", "/event-records")
                .as(BodyCodec.string())
                .sendJson(request, requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(400);

                    var error = JsonUtils.mapToObject(response.body(), ErrorResponse.class);
                    assertThat(error).isNotNull();
                    assertThat(error.getDetails()).hasSize(1);
                    assertThat(error.getDetails().get(0).getReason()).isEqualTo("events.db.notFound");
                    assertThat(error.getDetails().get(0).getAttributeName()).isEqualTo("entity");
                    assertThat(error.getDetails().get(0).getAttributeValues()).hasSize(1);
                    assertThat(error.getDetails().get(0).getAttributeValues().get(0)).isEqualTo("event");

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    @Test
    public void eventRecordCanBeCreated() throws Throwable {
        createDefaultUser();
        createDefaultEvent();

        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        var request = new CreateEventRecordRequest(defaultUser.getUserId(), defaultEvent.getId(), "event record");
        client
                .post(8080, "127.0.0.1", "/event-records")
                .as(BodyCodec.string())
                .sendJson(request, requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(200);

                    var body = JsonUtils.mapToObject(response.body(), EventRecordResponse.class);

                    assertThat(body.getId()).isNotNull();
                    assertThat(body.getUserId()).isEqualTo(defaultUser.getUserId());
                    assertThat(body.getEventId()).isEqualTo(defaultEvent.getId());
                    assertThat(body.getDescription()).isEqualTo("event record");

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    private void createDefaultEvent() throws Throwable {
        var now = ZonedDateTime.now();
        var event = EventModel.of(defaultUser.getUserId(), "demo subject", "demo description", now, now);

        var handler = server().getEventsRepo().save(event).get();
        handler.onSuccess(newEvent -> defaultEvent = newEvent);

        failOnError(handler);
    }

    private void createDefaultUser() throws Throwable {
        var handler = server().getUserRep().save("demo@example.com").get();
        handler.onSuccess(appUserModel -> defaultUser = appUserModel);

        failOnError(handler);
    }

}