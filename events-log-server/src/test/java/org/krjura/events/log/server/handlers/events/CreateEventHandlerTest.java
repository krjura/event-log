package org.krjura.events.log.server.handlers.events;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Test;
import org.krjura.events.log.server.support.BaseTest;
import org.krjura.events.log.server.model.AppUserModel;
import org.krjura.events.log.server.utils.JsonUtils;
import org.krjura.events.log.server.web.errors.ErrorResponse;
import org.krjura.events.log.server.web.events.CreateEventRequest;
import org.krjura.events.log.server.web.events.EventResponse;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateEventHandlerTest extends BaseTest {

    private AppUserModel defaultUser;

    @Test
    public void eventCannotBeCreatedIfUserIsNotFound() throws Exception {
        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        var request = new CreateEventRequest(1, "test subject", "test description");
        client
                .post(8080, "127.0.0.1", "/events")
                .as(BodyCodec.string())
                .sendJson(request, requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    assertThat(response.statusCode()).isEqualTo(400);

                    var error = JsonUtils.mapToObject(response.body(), ErrorResponse.class);
                    assertThat(error).isNotNull();
                    assertThat(error.getDetails()).hasSize(1);
                    assertThat(error.getDetails().get(0).getReason()).isEqualTo("events.db.notFound");
                    assertThat(error.getDetails().get(0).getAttributeName()).isEqualTo("entity");
                    assertThat(error.getDetails().get(0).getAttributeValues()).hasSize(1);
                    assertThat(error.getDetails().get(0).getAttributeValues().get(0)).isEqualTo("app_user");

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    @Test
    public void eventCanBeCreated() throws Throwable {
        createDefaultUser();

        var requestTestContext = new VertxTestContext();
        WebClient client = WebClient.create(Vertx.vertx());

        var request = new CreateEventRequest(defaultUser.getUserId(), "test subject", "test description");
        client
                .post(8080, "127.0.0.1", "/events")
                .as(BodyCodec.string())
                .sendJson(request, requestTestContext.succeeding(response -> requestTestContext.verify(() -> {
                    var body = JsonUtils.mapToObject(response.body(), EventResponse.class);

                    assertThat(body.getId()).isNotNull();
                    assertThat(body.getUserId()).isEqualTo(defaultUser.getUserId());
                    assertThat(body.getSubject()).isEqualTo("test subject");
                    assertThat(body.getDescription()).isEqualTo("test description");

                    requestTestContext.completeNow();
                })));

        assertThat(requestTestContext.awaitCompletion(5, TimeUnit.SECONDS)).isTrue();
        assertThat(requestTestContext.causeOfFailure()).isNull();
    }

    private void createDefaultUser() throws Throwable {
        var handler = server().getUserRep().save("demo@example.com").get();
        handler.onSuccess(appUserModel -> defaultUser = appUserModel);

        failOnError(handler);
    }
}
