package org.krjura.events.log.server;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.ResponseTimeHandler;
import org.krjura.events.log.server.config.ApplicationConfig;
import org.krjura.events.log.server.ex.ServerDetailedException;
import org.krjura.events.log.server.handlers.events.CreateEventHandler;
import org.krjura.events.log.server.handlers.events.DeleteEventHandler;
import org.krjura.events.log.server.handlers.events.ListEventsHandler;
import org.krjura.events.log.server.handlers.generic.JsonBodyHandler;
import org.krjura.events.log.server.handlers.generic.ParamCheckHandler;
import org.krjura.events.log.server.handlers.records.CreateEventRecordHandler;
import org.krjura.events.log.server.handlers.records.DeleteEventRecordHandler;
import org.krjura.events.log.server.handlers.records.ListEventRecordHandler;
import org.krjura.events.log.server.repository.AppUserRepository;
import org.krjura.events.log.server.repository.EventRecordRepository;
import org.krjura.events.log.server.repository.EventRepository;
import org.krjura.events.log.server.repository.PostgresConnector;
import org.krjura.events.log.server.web.events.CreateEventRequest;
import org.krjura.events.log.server.web.records.CreateEventRecordRequest;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.krjura.events.log.server.handlers.ParamConst.PARAM_EVENT_ID;
import static org.krjura.events.log.server.handlers.ParamConst.PARAM_EVENT_RECORD_ID;
import static org.krjura.events.log.server.handlers.ParamConst.PARAM_USER_ID;

public class EventsLogServer {

    private final Logger logger = LoggerFactory.getLogger(EventsLogServer.class);

    private HttpServer server;
    private Vertx vertx;

    private PostgresConnector connector;
    private AppUserRepository userRep;
    private EventRepository eventsRepo;
    private EventRecordRepository eventRecordRepo;

    public void start() {
        start(NoOpListenHandler.of());
    }

    public void start(Handler<AsyncResult<HttpServer>> listenHandler) {
        this.vertx = Vertx.vertx();

        try {
            var config = loadConfiguration(vertx).get();
            startServer(config, vertx, listenHandler);
        } catch (InterruptedException | ExecutionException e) {
            vertx.close();
            throw ServerDetailedException.of("events.config.error", "none", "", e);
        }
    }

    private CompletableFuture<ApplicationConfig> loadConfiguration(Vertx vertx) {
        var future = new CompletableFuture<ApplicationConfig>();

        var fileStore = new ConfigStoreOptions()
                .setType("file")
                .setConfig(new JsonObject().put("path", "application.json"));

        var options = new ConfigRetrieverOptions().addStore(fileStore);
        var retriever = ConfigRetriever.create(vertx, options);

        retriever.getConfig(configHandler -> {
            if(configHandler.succeeded()) {
                logger.info("configuration loaded", configHandler.cause());
                future.complete(configHandler.result().mapTo(ApplicationConfig.class));
            } else {
                logger.fatal("configuration of the system failed", configHandler.cause());
                future.completeExceptionally(configHandler.cause());
            }
        });

        return future;
    }

    private void startServer(ApplicationConfig config, Vertx vertx, Handler<AsyncResult<HttpServer>> listenHandler) {
        this.server = vertx.createHttpServer();

        var router = Router.router(vertx);
        this.server.requestHandler(router);

        this.connector = new PostgresConnector(config, vertx);
        this.userRep = new AppUserRepository(this.connector);
        this.eventsRepo = new EventRepository(this.connector);
        this.eventRecordRepo = new EventRecordRepository(this.connector);

        router
                .route(HttpMethod.POST, "/events")
                .consumes("application/json")
                .handler(ResponseTimeHandler.create())
                .handler(BodyHandler.create())
                .handler(JsonBodyHandler.of(CreateEventRequest.class))
                .handler(CreateEventHandler.of(userRep, eventsRepo));

        router
                .route(HttpMethod.GET, "/events/:userId")
                .handler(ResponseTimeHandler.create())
                .handler(ParamCheckHandler.of(PARAM_USER_ID))
                .handler(ListEventsHandler.of(eventsRepo));

        router
                .route(HttpMethod.DELETE, "/events/:userId/:eventId")
                .handler(ResponseTimeHandler.create())
                .handler(ParamCheckHandler.of(PARAM_USER_ID, PARAM_EVENT_ID))
                .handler(DeleteEventHandler.of(eventsRepo));

        router
                .route(HttpMethod.POST, "/event-records")
                .consumes("application/json")
                .handler(ResponseTimeHandler.create())
                .handler(BodyHandler.create())
                .handler(JsonBodyHandler.of(CreateEventRecordRequest.class))
                .handler(CreateEventRecordHandler.of(eventsRepo, eventRecordRepo));

        router
                .route(HttpMethod.DELETE, "/event-records/:userId/:eventId/:eventRecordId")
                .handler(ResponseTimeHandler.create())
                .handler(ParamCheckHandler.of(PARAM_USER_ID, PARAM_EVENT_ID, PARAM_EVENT_RECORD_ID))
                .handler(DeleteEventRecordHandler.of(eventRecordRepo));

        router
                .route(HttpMethod.GET, "/event-records/:userId/:eventId")
                .handler(ResponseTimeHandler.create())
                .handler(ParamCheckHandler.of(PARAM_USER_ID, PARAM_EVENT_ID))
                .handler(ListEventRecordHandler.of(eventRecordRepo));

        this.server.listen(8080, listenHandler);
    }

    public PostgresConnector getConnector() {
        return connector;
    }

    public AppUserRepository getUserRep() {
        return userRep;
    }

    public EventRepository getEventsRepo() {
        return eventsRepo;
    }

    public EventRecordRepository getEventRecordRepo() {
        return eventRecordRepo;
    }

    public void close() {
        this.server.close();
        this.vertx.close();
    }

    public static void main(String[] args) {
        var server = new EventsLogServer();
        server.start();
    }
}
