package org.krjura.events.log.server.handlers.utils;

import io.vertx.ext.web.RoutingContext;
import org.krjura.events.log.server.ex.ServerDetailedException;
import org.krjura.events.log.server.utils.JsonUtils;
import org.krjura.events.log.server.web.errors.ErrorDetails;
import org.krjura.events.log.server.web.errors.ErrorResponse;

import static java.util.Objects.requireNonNull;

public class ResponseUtils {

    public static void onErrorResponse(RoutingContext context, Throwable throwable) {
        requireNonNull(context);
        requireNonNull(throwable);

        context.response().setStatusCode(400);

        if(throwable instanceof ServerDetailedException) {
            var ex = (ServerDetailedException) throwable;

            context.response().putHeader("content-type", "application/json");
            context.response().end(JsonUtils.mapToString(ex.getResponse()));
        } else {
            context.response().putHeader("content-type", "text/plain");
            context.response().end(throwable.getMessage());
        }
    }

    public static void onJsonbResult(RoutingContext context, Object val) {
        requireNonNull(context);
        requireNonNull(val);

        context.response().putHeader("content-type", "application/json");
        context.response().end(JsonUtils.mapToString(val));
    }

    public static void onNoContent(RoutingContext context) {
        requireNonNull(context);

        context.response().setStatusCode(204);
        context.response().end();
    }

    public static void onParamErrorResponse(RoutingContext context, String reason, String param, String value) {
        requireNonNull(context);
        requireNonNull(reason);
        requireNonNull(param);
        requireNonNull(value);

        context.response().setStatusCode(400);
        context.response().putHeader("content-type", "application/json");
        context.response().end(
                JsonUtils.mapToString(ErrorResponse.of(ErrorDetails.of(reason, param, value)))
        );
    }
}
