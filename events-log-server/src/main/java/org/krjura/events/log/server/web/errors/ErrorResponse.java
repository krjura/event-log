package org.krjura.events.log.server.web.errors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse {

    private List<ErrorDetails> details;

    public static ErrorResponse of(ErrorDetails details) {
        Objects.requireNonNull(details);

        var response = new ErrorResponse();
        response.setDetails(List.of(details));
        return response;
    }

    public List<ErrorDetails> getDetails() {
        if( this.details == null ) {
            this.details = new ArrayList<>(1);
        }

        return details;
    }

    public void setDetails(List<ErrorDetails> details) {
        this.details = details;
    }

    public void addDetail(ErrorDetails errorDetails) {
        getDetails().add(errorDetails);
    }

    @JsonIgnore
    public boolean isEmpty() {
        return getDetails().isEmpty();
    }

    @Override
    public String toString() {
        return "ErrorResponseData{" +
                "details=" + details +
                '}';
    }
}
