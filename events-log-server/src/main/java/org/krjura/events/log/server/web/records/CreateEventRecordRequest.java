package org.krjura.events.log.server.web.records;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import static java.util.Objects.requireNonNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateEventRecordRequest {

    private final Integer userId;

    private final Integer eventId;

    private final String description;

    @JsonCreator
    public CreateEventRecordRequest(
            @JsonProperty("userId") Integer userId,
            @JsonProperty("eventId") Integer eventId,
            @JsonProperty("description") String description) {

        this.userId = requireNonNull(userId);
        this.eventId = requireNonNull(eventId);
        this.description = requireNonNull(description);
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public String getDescription() {
        return description;
    }
}
