package org.krjura.events.log.server.handlers.utils;

import io.vertx.ext.web.RoutingContext;

import static java.util.Objects.requireNonNull;

public class PathParamsUtils {

    public static Integer paramAsInteger(RoutingContext context, String param) {
        requireNonNull(context);
        requireNonNull(param);

        String val =  context.request().getParam(param);

        if(val == null) {
            return null;
        }

        try {
            return Integer.parseInt(val);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
