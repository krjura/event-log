package org.krjura.events.log.server.handlers.generic;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.krjura.events.log.server.handlers.utils.PathParamsUtils.paramAsInteger;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onParamErrorResponse;

public class ParamCheckHandler implements Handler<RoutingContext> {

    private final List<String> params;

    public ParamCheckHandler(List<String> params) {
        this.params = requireNonNull(params);
    }

    public static ParamCheckHandler of(String... params) {
        return new ParamCheckHandler(List.of(params));
    }

    @Override
    public void handle(RoutingContext context) {

        for(String key : params) {
            var val = paramAsInteger(context, key);

            if(val == null) {
                onParamErrorResponse(context, "events.web.param.notFound", key, "null");
                return;
            }
        }

        context.next();
    }
}
