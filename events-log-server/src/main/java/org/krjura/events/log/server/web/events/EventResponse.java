package org.krjura.events.log.server.web.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventResponse {

    private final Integer id;

    private final Integer userId;

    private final String subject;

    private final String description;

    @JsonCreator
    public EventResponse(
            @JsonProperty("id") final Integer id,
            @JsonProperty("userId") final Integer userId,
            @JsonProperty("subject")final String subject,
            @JsonProperty("description")final String description) {

        this.id = Objects.requireNonNull(id);
        this.userId = Objects.requireNonNull(userId);
        this.subject = Objects.requireNonNull(subject);
        this.description = Objects.requireNonNull(description);
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventResponse that = (EventResponse) o;
        return id.equals(that.id) &&
                userId.equals(that.userId) &&
                subject.equals(that.subject) &&
                description.equals(that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, subject, description);
    }

    @Override
    public String toString() {
        return "CreateEventRequest{" +
                "userId=" + userId +
                ", subject='" + subject + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}