package org.krjura.events.log.server.web.events;

import org.krjura.events.log.server.model.EventModel;

import java.util.List;
import java.util.stream.Collectors;

public class EventResponseHelper {

    public static ListEventResponse buildEventsResponse(List<EventModel> responses) {
        var list =  responses
                .stream()
                .map(EventResponseHelper::buildEventResponse)
                .collect(Collectors.toUnmodifiableList());

        return new ListEventResponse(list);
    }

    public static EventResponse buildEventResponse(EventModel response) {
        return new EventResponse(
                response.getId(), response.getUserId(), response.getSubject(), response.getDescription()
        );
    }
}
