package org.krjura.events.log.server;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

public class NoOpListenHandler implements Handler<AsyncResult<HttpServer>> {

    private Logger logger = LoggerFactory.getLogger(NoOpListenHandler.class);

    public static NoOpListenHandler of() {
        return new NoOpListenHandler();
    }

    @Override
    public void handle(AsyncResult<HttpServer> event) {
        RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();

        logger.info("System running for " + rb.getUptime());
    }
}
