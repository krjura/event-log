package org.krjura.events.log.server.repository;

import io.vertx.core.Vertx;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import java.util.Objects;
import org.krjura.events.log.server.config.ApplicationConfig;

public class PostgresConnector {

    private final PgPool client;

    public PostgresConnector(ApplicationConfig config, Vertx vertx) {
        Objects.requireNonNull(config);
        Objects.requireNonNull(vertx);

        client = initClient(config, vertx);
    }

    private PgPool initClient(ApplicationConfig config, Vertx vertx) {

        PgConnectOptions connectOptions = new PgConnectOptions()
                .setPort(config.getPostgreSqlConfig().getPort())
                .setHost(config.getPostgreSqlConfig().getHost())
                .setDatabase(config.getPostgreSqlConfig().getDatabase())
                .setUser(config.getPostgreSqlConfig().getUser())
                .setPassword(config.getPostgreSqlConfig().getPassword());

        PoolOptions poolOptions = new PoolOptions()
                .setMaxSize(5);

        return PgPool.pool(vertx, connectOptions, poolOptions);
    }

    public PgPool getClient() {
        return client;
    }
}
