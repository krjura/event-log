package org.krjura.events.log.server.handlers.records;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import org.krjura.events.log.server.repository.EventRecordRepository;
import org.krjura.events.log.server.utils.Either;
import org.krjura.events.log.server.utils.TypeRef;

import static java.util.Objects.requireNonNull;
import static org.krjura.events.log.server.handlers.ParamConst.PARAM_EVENT_RECORD_ID;
import static org.krjura.events.log.server.handlers.ParamConst.PARAM_USER_ID;
import static org.krjura.events.log.server.handlers.utils.PathParamsUtils.paramAsInteger;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onErrorResponse;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onNoContent;

public class DeleteEventRecordHandler implements Handler<RoutingContext> {

    private final EventRecordRepository eventRecordRepo;

    public DeleteEventRecordHandler(EventRecordRepository eventRecordRepo) {
        this.eventRecordRepo = requireNonNull(eventRecordRepo);
    }

    public static DeleteEventRecordHandler of(EventRecordRepository eventRecordRepo) {
        return new DeleteEventRecordHandler(eventRecordRepo);
    }

    @Override
    public void handle(RoutingContext ctx) {

        // use ParamCheckHandler to ensure param exists
        var userId = paramAsInteger(ctx, PARAM_USER_ID);
        var eventRecordId = paramAsInteger(ctx, PARAM_EVENT_RECORD_ID);

        eventRecordRepo
                .delete(userId, eventRecordId)
                .exceptionally(throwable -> Either.error(new TypeRef<>(), throwable))
                .thenAccept(it -> {
                    it.onSuccess(events -> onNoContent(ctx));
                    it.onError(throwable -> onErrorResponse(ctx, throwable));
                });
    }
}
