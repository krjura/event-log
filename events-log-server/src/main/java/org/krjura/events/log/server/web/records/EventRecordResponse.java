package org.krjura.events.log.server.web.records;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventRecordResponse {

    private final Integer id;

    private final Integer userId;

    private final Integer eventId;

    private final String description;

    @JsonCreator
    public EventRecordResponse(
            @JsonProperty("id") final Integer id,
            @JsonProperty("userId") final Integer userId,
            @JsonProperty("eventId") final Integer eventId,
            @JsonProperty("description") final String description) {

        this.id = Objects.requireNonNull(id);
        this.userId = Objects.requireNonNull(userId);
        this.eventId = Objects.requireNonNull(eventId);
        this.description = Objects.requireNonNull(description);
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "EventRecordResponse{" +
                "id=" + id +
                ", userId=" + userId +
                ", eventId=" + eventId +
                ", description='" + description + '\'' +
                '}';
    }
}