package org.krjura.events.log.server.utils;

public class EitherType<T> {

    private final TypeRef<T> typeRef;

    public EitherType(Class<T> clazz) {
        this.typeRef = new TypeRef<T>();
    }

    public EitherType(TypeRef<T> typeRef) {
        this.typeRef = typeRef;
    }

    public Either<T> error(Throwable throwable) {
        return new Either<>(null, throwable);
    }

    public Either<T> success(T data) {
        return new Either<>(data, null);
    }
}
