package org.krjura.events.log.server.model;

import java.util.Objects;

public class AppUserModel {

    private final Integer userId;

    private final String email;

    public AppUserModel(final Integer userId, final String email) {
        this.userId = userId;
        this.email = Objects.requireNonNull(email);
    }

    public Integer getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "AppUser{" +
                "userId=" + userId +
                ", email='" + email + '\'' +
                '}';
    }
}
