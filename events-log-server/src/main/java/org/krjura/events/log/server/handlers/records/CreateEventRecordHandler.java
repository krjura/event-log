package org.krjura.events.log.server.handlers.records;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import org.krjura.events.log.server.enums.ServerConst;
import org.krjura.events.log.server.model.EventModel;
import org.krjura.events.log.server.model.EventRecord;
import org.krjura.events.log.server.repository.EventRecordRepository;
import org.krjura.events.log.server.repository.EventRepository;
import org.krjura.events.log.server.utils.Either;
import org.krjura.events.log.server.web.records.CreateEventRecordRequest;

import java.time.ZonedDateTime;
import java.util.concurrent.CompletableFuture;

import static java.util.Objects.requireNonNull;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onErrorResponse;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onJsonbResult;
import static org.krjura.events.log.server.web.records.EventRecordResponseHelper.buildResponse;

public class CreateEventRecordHandler implements Handler<RoutingContext>  {

    private final EventRepository eventsRepo;

    private final EventRecordRepository eventRecordRepo;

    public CreateEventRecordHandler(EventRepository eventsRepo, EventRecordRepository eventRecordRepo) {
        this.eventsRepo = requireNonNull(eventsRepo);
        this.eventRecordRepo = requireNonNull(eventRecordRepo);
    }

    public static CreateEventRecordHandler of(EventRepository eventsRepo, EventRecordRepository eventRecordRepo) {
        return new CreateEventRecordHandler(eventsRepo, eventRecordRepo);
    }

    @Override
    public void handle(RoutingContext ctx) {

        var request = (CreateEventRecordRequest) ctx.get(ServerConst.CONST_PROPS_OBJECT);

        // 1 ensure event exists and belong to user
        // 2 save event record
        eventsRepo
                .findByUserIdAndEventId(request.getUserId(), request.getEventId())
                .exceptionally(th -> Either.error(EventModel.class, th))
                .thenCompose(it -> createEventRecord(request, it))
                .thenAccept(it -> {
                    it.onSuccess(event -> onJsonbResult(ctx, buildResponse(event)));
                    it.onError(throwable -> onErrorResponse(ctx, throwable));
                });
    }

    private CompletableFuture<Either<EventRecord>> createEventRecord(
            CreateEventRecordRequest request, Either<EventModel> eventResult) {

        if(eventResult.isError()) {
            return CompletableFuture.completedFuture(Either.error(EventRecord.class, eventResult.getError()));
        }

        var now = ZonedDateTime.now();
        var record = EventRecord.of(request.getUserId(), request.getEventId(), request.getDescription(), now, now);

        return eventRecordRepo.save(record);
    }
}
