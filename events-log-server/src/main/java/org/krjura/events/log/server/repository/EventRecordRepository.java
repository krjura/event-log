package org.krjura.events.log.server.repository;

import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.Tuple;
import org.krjura.events.log.server.model.EventRecord;
import org.krjura.events.log.server.utils.Either;
import org.krjura.events.log.server.utils.EitherType;
import org.krjura.events.log.server.utils.TypeRef;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import static java.util.Objects.requireNonNull;
import static org.krjura.events.log.server.handlers.utils.PagingUtils.offset;
import static org.krjura.events.log.server.repository.RepoTools.extractId;

public class EventRecordRepository {

    private static final String QUERIES_INSERT = "INSERT INTO event_record " +
            "(user_id, event_id, description, created_at, modified_at) " +
            "VALUES ($1, $2, $3, $4, $5) RETURNING id";

    private static final String QUERIES_SELECT =
            "SELECT id, user_id, event_id, description, created_at, modified_at FROM event_record " +
                    "WHERE user_id = $1 AND event_id = $2 LIMIT $3 OFFSET $4";

    private static final String QUERIES_DELETE = "DELETE FROM event_record WHERE user_id = $1 AND id = $2";

    private static final EitherType<EventRecord> EITHER = new EitherType<>(EventRecord.class);
    private static final EitherType<List<EventRecord>> EITHER_LIST = new EitherType<>(new TypeRef<>());
    private static final EitherType<Integer> EITHER_VOID = new EitherType<>(new TypeRef<>());

    private final PostgresConnector connector;

    public EventRecordRepository(PostgresConnector connector) {
        this.connector = requireNonNull(connector);
    }

    public CompletableFuture<Either<EventRecord>> save(EventRecord entity) {
        requireNonNull(entity);

        var future = new CompletableFuture<Either<EventRecord>>();

        var params = Tuple.of(
                entity.getUserId(),
                entity.getEventId(),
                entity.getDescription(),
                entity.getCreatedAt().toOffsetDateTime(),
                entity.getModifiedAt().toOffsetDateTime()
        );

        connector
                .getClient()
                .preparedQuery(QUERIES_INSERT, params, callback -> {

                    if(callback.succeeded()) {
                        var id = extractId(callback.result());

                        var event = EventRecord.of(
                                id, entity.getUserId(), entity.getEventId(), entity.getDescription(),
                                entity.getCreatedAt(), entity.getModifiedAt()
                        );

                        future.complete(EITHER.success(event));
                    } else {
                        future.complete(EITHER.error(callback.cause()));
                    }
                });

        return future;
    }

    public CompletableFuture<Either<List<EventRecord>>> list(Integer userId, Integer eventId, int page, int size) {
        requireNonNull(userId);

        var future = new CompletableFuture<Either<List<EventRecord>>>();

        var params = Tuple.of(userId, eventId, size, offset(page, size));

        connector
                .getClient()
                .preparedQuery(QUERIES_SELECT, params, callback -> {

                    if(callback.succeeded()) {
                        future.complete(EITHER_LIST.success(mapRows(callback.result())));
                    } else {
                        future.complete(EITHER_LIST.error(callback.cause()));
                    }
                });

        return future;
    }

    @SuppressWarnings("Duplicates")
    public CompletableFuture<Either<Integer>> delete(Integer userId, Integer eventRecordId) {
        requireNonNull(userId);
        requireNonNull(eventRecordId);

        var future = new CompletableFuture<Either<Integer>>();

        var params = Tuple.of(userId, eventRecordId);

        connector
                .getClient()
                .preparedQuery(QUERIES_DELETE, params, callback -> {

                    if(callback.succeeded()) {
                        future.complete(EITHER_VOID.success(eventRecordId));
                    } else {
                        future.complete(EITHER_VOID.error(callback.cause()));
                    }
                });

        return future;
    }

    private static List<EventRecord> mapRows(RowSet<Row> rows) {
        var list = new ArrayList<EventRecord>(rows.size());

        for(Row row : rows) {
            list.add(mapRow(row));
        }

        return list;
    }

    private static EventRecord mapRow(Row row) {
        return new EventRecord(
                row.getInteger("id"),
                row.getInteger("user_id"),
                row.getInteger("event_id"),
                row.getString("description"),
                row.getOffsetDateTime("created_at").toZonedDateTime(),
                row.getOffsetDateTime("modified_at").toZonedDateTime()
        );
    }
}
