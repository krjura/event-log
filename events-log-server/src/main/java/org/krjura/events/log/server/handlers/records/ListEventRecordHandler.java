package org.krjura.events.log.server.handlers.records;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import org.krjura.events.log.server.repository.EventRecordRepository;
import org.krjura.events.log.server.utils.Either;
import org.krjura.events.log.server.utils.TypeRef;

import static java.util.Objects.requireNonNull;
import static org.krjura.events.log.server.handlers.ParamConst.PARAM_EVENT_ID;
import static org.krjura.events.log.server.handlers.ParamConst.PARAM_USER_ID;
import static org.krjura.events.log.server.handlers.utils.PagingUtils.extractPage;
import static org.krjura.events.log.server.handlers.utils.PagingUtils.extractSize;
import static org.krjura.events.log.server.handlers.utils.PathParamsUtils.paramAsInteger;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onErrorResponse;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onJsonbResult;
import static org.krjura.events.log.server.web.records.EventRecordResponseHelper.buildResponse;

public class ListEventRecordHandler implements Handler<RoutingContext> {

    private final EventRecordRepository eventRecordRepo;

    public ListEventRecordHandler(EventRecordRepository eventRecordRepo) {
        this.eventRecordRepo = requireNonNull(eventRecordRepo);
    }

    public static ListEventRecordHandler of(EventRecordRepository eventRecordRepo) {
        return new ListEventRecordHandler(eventRecordRepo);
    }

    @Override
    public void handle(RoutingContext ctx) {
        var page = extractPage(ctx, 0);
        var size = extractSize(ctx, 50);

        // use ParamCheckHandler to ensure param exists
        var userId = paramAsInteger(ctx, PARAM_USER_ID);
        var eventId = paramAsInteger(ctx, PARAM_EVENT_ID);

        eventRecordRepo
                .list(userId, eventId, page, size)
                .exceptionally(throwable -> Either.error(new TypeRef<>(), throwable))
                .thenAccept(it -> {
                    it.onSuccess(events -> onJsonbResult(ctx, buildResponse(events)));
                    it.onError(throwable -> onErrorResponse(ctx, throwable));
                });
    }
}
