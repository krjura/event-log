package org.krjura.events.log.server.model;

import java.time.ZonedDateTime;

import static java.util.Objects.requireNonNull;

public class EventRecord {

    private final Integer id;

    private final Integer userId;

    private final Integer eventId;

    private final String description;

    private final ZonedDateTime createdAt;

    private final ZonedDateTime modifiedAt;

    public EventRecord(
            Integer id,
            Integer userId, Integer eventId,
            String description, ZonedDateTime createdAt, ZonedDateTime modifiedAt) {

        this.id = id;
        this.userId = requireNonNull(userId);
        this.eventId = requireNonNull(eventId);
        this.description = requireNonNull(description);
        this.createdAt = requireNonNull(createdAt);
        this.modifiedAt = requireNonNull(modifiedAt);
    }

    public static EventRecord of(
            Integer userId, Integer eventId,
            String description, ZonedDateTime createdAt, ZonedDateTime modifiedAt) {

        return new EventRecord(null, userId, eventId, description, createdAt, modifiedAt);
    }

    public static EventRecord of(
            Integer id,
            Integer userId, Integer eventId,
            String description, ZonedDateTime createdAt, ZonedDateTime modifiedAt) {

        return new EventRecord(id, userId, eventId, description, createdAt, modifiedAt);
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public String getDescription() {
        return description;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public ZonedDateTime getModifiedAt() {
        return modifiedAt;
    }
}