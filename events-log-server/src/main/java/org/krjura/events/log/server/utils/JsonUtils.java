package org.krjura.events.log.server.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import org.krjura.events.log.server.ex.ServerDetailedException;

public class JsonUtils {

    public static ObjectMapper objectMapper;

    public static ObjectMapper objectMapper() {
        if(objectMapper == null) {
            ObjectMapper mapper = new ObjectMapper();

            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

            mapper.registerModule(new AfterburnerModule());

            objectMapper = mapper;
        }

        return objectMapper;
    }

    public static String mapToString(Object data) {
        try {
            return objectMapper().writeValueAsString(data);
        } catch (JsonProcessingException e) {
            throw ServerDetailedException.of("events.json.serialization.error", "body", "", e);
        }
    }

    public static <T> T mapToObject(String body, Class<T> clazz) {
        try {
            return objectMapper().readValue(body, clazz);
        } catch (JsonProcessingException e) {
            throw ServerDetailedException.of("events.json.deserialization.error", "class", clazz.getSimpleName(), e);
        }
    }
}
