package org.krjura.events.log.server.config;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class PostgreSqlConfig {

    private final Integer port;

    private final String host;

    private final String database;

    private final String user;

    private final String password;

    @JsonCreator
    public PostgreSqlConfig(
            @JsonProperty("port") Integer port,
            @JsonProperty("host") String host,
            @JsonProperty("database") String database,
            @JsonProperty("user") String user,
            @JsonProperty("password") String password) {

        this.port = Objects.requireNonNull(port);
        this.host = Objects.requireNonNull(host);
        this.database = Objects.requireNonNull(database);
        this.user = Objects.requireNonNull(user);
        this.password = Objects.requireNonNull(password);
    }

    public Integer getPort() {
        return port;
    }

    public String getHost() {
        return host;
    }

    public String getDatabase() {
        return database;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
