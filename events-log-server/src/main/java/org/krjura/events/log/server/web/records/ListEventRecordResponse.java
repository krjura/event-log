package org.krjura.events.log.server.web.records;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import static java.util.Objects.requireNonNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListEventRecordResponse {

    private List<EventRecordResponse> records;

    @JsonCreator
    public ListEventRecordResponse(
            @JsonProperty("records") List<EventRecordResponse> records) {

        this.records = requireNonNull(records);
    }

    public List<EventRecordResponse> getRecords() {
        return records;
    }
}
