package org.krjura.events.log.server.handlers;

public class ParamConst {

    public static final String PARAM_USER_ID = "userId";
    public static final String PARAM_EVENT_ID = "eventId";
    public static final String PARAM_EVENT_RECORD_ID = "eventRecordId";

    private ParamConst() {
        // const
    }
}
