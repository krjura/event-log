package org.krjura.events.log.server.handlers.utils;

import io.vertx.ext.web.RoutingContext;

import static java.util.Objects.requireNonNull;

public class PagingUtils {

    public static int extractPage(RoutingContext context, int defaultPage) {
        requireNonNull(context);

        var page = context.request().getParam("page");

        if(page == null) {
            return defaultPage;
        }

        try {
            int parsedPage = Integer.parseInt(page);

            return parsedPage >=0 ? parsedPage : defaultPage;
        } catch (NumberFormatException e) {
            return defaultPage;
        }
    }

    public static int extractSize(RoutingContext context, int maxSize) {
        requireNonNull(context);

        var size = context.request().getParam("size");

        if(size == null) {
            return maxSize;
        }

        try {
            int parsedPage = Integer.parseInt(size);

            if(parsedPage < 1) {
                parsedPage = maxSize;
            }

            return Math.min(parsedPage, maxSize);
        } catch (NumberFormatException e) {
            return maxSize;
        }
    }

    public static int offset(int page, int size) {
        return page * size;
    }
}
