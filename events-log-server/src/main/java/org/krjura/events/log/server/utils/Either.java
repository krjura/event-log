package org.krjura.events.log.server.utils;

import java.util.function.Consumer;

public class Either<T> {

    private T data;

    private Throwable error;

    public Either(T data, Throwable error) {
        this.data = data;
        this.error = error;
    }

    public static <T> Either<T> error(Class<T> clazz, Throwable throwable) {
        return new Either<>(null, throwable);
    }

    public static <T> Either<T> error(TypeRef<T> typeRef, Throwable throwable) {
        return new Either<>(null, throwable);
    }

    public static <T> Either<T> success(T data) {
        return new Either<>(data, null);
    }

    public void onSuccess(Consumer<T> consumer) {
        if(data != null) {
            consumer.accept(data);
        }
    }

    public void onError(Consumer<Throwable> consumer) {
        if(error == null) {
            return;
        }

        consumer.accept(error);
    }

    public T getData() {
        return data;
    }

    public Throwable getError() {
        return error;
    }

    public boolean isError() {
        return error != null;
    }
}
