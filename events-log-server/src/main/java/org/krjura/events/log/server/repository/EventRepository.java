package org.krjura.events.log.server.repository;

import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.krjura.events.log.server.ex.ServerDetailedException;
import org.krjura.events.log.server.model.EventModel;
import org.krjura.events.log.server.utils.Either;

import java.util.Objects;

import org.krjura.events.log.server.utils.EitherType;
import org.krjura.events.log.server.utils.TypeRef;

import static java.util.Objects.requireNonNull;
import static org.krjura.events.log.server.handlers.utils.PagingUtils.offset;
import static org.krjura.events.log.server.repository.RepoTools.extractId;

public class EventRepository {

    public static final String QUERIES_INSERT = "INSERT INTO event " +
            "(user_id, subject, description, created_at, modified_at) " +
            "VALUES ($1, $2, $3, $4, $5)  RETURNING id";

    private static final String QUERIES_SELECT =
            "SELECT id, user_id, subject, description, created_at, modified_at FROM event " +
            "WHERE user_id = $1 LIMIT $2 OFFSET $3";

    private static final String QUERIES_FIND_BY_USER_ID_AND_EVENT_ID =
            "SELECT id, user_id, subject, description, created_at, modified_at FROM event " +
                    "WHERE user_id = $1 AND id = $2";

    private static final String QUERIES_DELETE = "DELETE FROM event WHERE user_id = $1 AND id = $2";

    private static final EitherType<EventModel> EITHER = new EitherType<>(EventModel.class);
    private static final EitherType<List<EventModel>> EITHER_LIST = new EitherType<>(new TypeRef<>());
    private static final EitherType<Integer> EITHER_VOID = new EitherType<>(new TypeRef<>());

    private final PostgresConnector connector;

    public EventRepository(PostgresConnector connector) {
        this.connector = requireNonNull(connector);
    }

    public CompletableFuture<Either<EventModel>> save(EventModel entity) {
        requireNonNull(entity);

        var future = new CompletableFuture<Either<EventModel>>();

        var params = Tuple.of(
                entity.getUserId(),
                entity.getSubject(),
                entity.getDescription(),
                entity.getCreatedAt().toOffsetDateTime(),
                entity.getModifiedAt().toOffsetDateTime()
        );

        connector
                .getClient()
                .preparedQuery(QUERIES_INSERT, params, callback -> {

            if(callback.succeeded()) {
                var id = extractId(callback.result());

                var event = EventModel.of(
                        id, entity.getUserId(), entity.getSubject(), entity.getDescription(),
                        entity.getCreatedAt(), entity.getModifiedAt()
                );

                future.complete(EITHER.success(event));
            } else {
                future.complete(EITHER.error(callback.cause()));
            }
        });

        return future;
    }

    public CompletableFuture<Either<List<EventModel>>> list(Integer userId, int page, int size) {
        requireNonNull(userId);

        var future = new CompletableFuture<Either<List<EventModel>>>();

        var params = Tuple.of(userId, size, offset(page, size));

        connector
                .getClient()
                .preparedQuery(QUERIES_SELECT, params, callback -> {

                    if(callback.succeeded()) {
                        future.complete(EITHER_LIST.success(mapRows(callback.result())));
                    } else {
                        future.complete(EITHER_LIST.error(callback.cause()));
                    }
                });

        return future;
    }

    public CompletableFuture<Either<EventModel>> findByUserIdAndEventId(Integer userId, Integer eventId) {
        requireNonNull(userId);

        var future = new CompletableFuture<Either<EventModel>>();

        var params = Tuple.of(userId, eventId);

        connector
                .getClient()
                .preparedQuery(QUERIES_FIND_BY_USER_ID_AND_EVENT_ID, params, callback -> {
                    if(callback.succeeded()) {
                        future.complete(mapSingleRow(callback.result()));
                    } else {
                        future.complete(EITHER.error(callback.cause()));
                    }
                });


        return future;
    }

    public CompletableFuture<Either<Integer>> delete(Integer userId, Integer eventId) {
        requireNonNull(userId);

        var future = new CompletableFuture<Either<Integer>>();

        var params = Tuple.of(userId, eventId);

        connector
                .getClient()
                .preparedQuery(QUERIES_DELETE, params, callback -> {

                    if(callback.succeeded()) {
                        future.complete(EITHER_VOID.success(eventId));
                    } else {
                        future.complete(EITHER_VOID.error(callback.cause()));
                    }
                });

        return future;
    }

    private static Either<EventModel> mapSingleRow(RowSet<Row> rows) {
        if(rows.rowCount() == 0) {
            return EITHER.error(ServerDetailedException.of("events.db.notFound", "entity", "event"));
        } else if(rows.rowCount() > 1) {
            return EITHER.error(ServerDetailedException.of("events.db.nonUnique", "entity", "event"));
        }

        var iterator = rows.iterator();
        var row = iterator.next();

        return EITHER.success(mapRow(row));
    }

    private static List<EventModel> mapRows(RowSet<Row> rows) {
        var list = new ArrayList<EventModel>(rows.size());

        for(Row row : rows) {
            list.add(mapRow(row));
        }

        return list;
    }

    private static EventModel mapRow(Row row) {
        return new EventModel(
                row.getInteger("id"),
                row.getInteger("user_id"),
                row.getString("subject"),
                row.getString("description"),
                row.getOffsetDateTime("created_at").toZonedDateTime(),
                row.getOffsetDateTime("modified_at").toZonedDateTime()
        );
    }
}
