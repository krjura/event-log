package org.krjura.events.log.server.handlers.events;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import org.krjura.events.log.server.repository.EventRepository;
import org.krjura.events.log.server.utils.Either;
import org.krjura.events.log.server.utils.TypeRef;

import java.util.Objects;

import static org.krjura.events.log.server.handlers.ParamConst.PARAM_EVENT_ID;
import static org.krjura.events.log.server.handlers.ParamConst.PARAM_USER_ID;
import static org.krjura.events.log.server.handlers.utils.PathParamsUtils.paramAsInteger;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onErrorResponse;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onNoContent;

public class DeleteEventHandler implements Handler<RoutingContext>  {

    private final EventRepository eventsRepo;

    public DeleteEventHandler(EventRepository eventsRepo) {
        this.eventsRepo = Objects.requireNonNull(eventsRepo);
    }

    public static Handler<RoutingContext> of(EventRepository eventsRepo) {
        return new DeleteEventHandler(eventsRepo);
    }

    @Override
    public void handle(RoutingContext ctx) {

        // use ParamCheckHandler to ensure param exists
        var userId = paramAsInteger(ctx, PARAM_USER_ID);
        var eventId = paramAsInteger(ctx, PARAM_EVENT_ID);

        eventsRepo
                .delete(userId, eventId)
                .exceptionally(throwable -> Either.error(new TypeRef<>(), throwable))
                .thenAccept(it -> {
                    it.onSuccess(events -> onNoContent(ctx));
                    it.onError(throwable -> onErrorResponse(ctx, throwable));
                });
    }
}
