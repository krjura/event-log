package org.krjura.events.log.server.web.records;

import org.krjura.events.log.server.model.EventRecord;

import java.util.List;
import java.util.stream.Collectors;

public class EventRecordResponseHelper {

    public static ListEventRecordResponse buildResponse(List<EventRecord> responses) {
        var list = responses
                .stream()
                .map(EventRecordResponseHelper::buildResponse)
                .collect(Collectors.toUnmodifiableList());

        return new ListEventRecordResponse(list);
    }

    public static EventRecordResponse buildResponse(EventRecord response) {
        return new EventRecordResponse(
                response.getId(), response.getUserId(), response.getEventId(), response.getDescription()
        );
    }
}
