package org.krjura.events.log.server.web.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateEventRequest {

    private final Integer userId;

    private final String subject;

    private final String description;

    @JsonCreator
    public CreateEventRequest(
            @JsonProperty("userId") final Integer userId,
            @JsonProperty("subject")final String subject,
            @JsonProperty("description")final String description) {

        this.userId = userId;
        this.subject = subject;
        this.description = description;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "CreateEventRequest{" +
                "userId=" + userId +
                ", subject='" + subject + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}