package org.krjura.events.log.server.ex;

import org.krjura.events.log.server.web.errors.ErrorDetails;
import org.krjura.events.log.server.web.errors.ErrorResponse;

public class ServerDetailedException extends RuntimeException {

    private final ErrorResponse response;

    public ServerDetailedException(ErrorResponse response) {
        super("error occurred");

        this.response = response;
    }

    public ServerDetailedException(ErrorResponse response, Throwable cause) {
        super("error occurred");

        this.response = response;
    }

    public static ServerDetailedException of(String reason, String attributeName, String attributeValue) {
        return new ServerDetailedException(
                ErrorResponse.of(ErrorDetails.of(reason, attributeName, attributeValue))
        );
    }

    public static ServerDetailedException of(String reason, String attributeName, String attributeValue, Throwable th) {
        return new ServerDetailedException(
                ErrorResponse.of(ErrorDetails.of(reason, attributeName, attributeValue)),
                th
        );
    }

    public ErrorResponse getResponse() {
        return response;
    }
}
