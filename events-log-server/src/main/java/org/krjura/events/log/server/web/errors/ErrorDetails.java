package org.krjura.events.log.server.web.errors;

import java.util.ArrayList;
import java.util.List;

public class ErrorDetails {

    // required
    private String reason;

    // optional
    private String attributeName;

    // optional
    private List<String> attributeValues;

    public ErrorDetails() {
        this.attributeValues = new ArrayList<>(1);
    }

    public static ErrorDetails of(String reason, String attributeName, String attributeValue) {
        return of(reason, attributeName, List.of(attributeValue));
    }

    public static ErrorDetails of(String reason, String attributeName, List<String> attributeValue) {
        var details = new ErrorDetails();

        details.setReason(reason);
        details.setAttributeName(attributeName);
        details.setAttributeValues(attributeValue);

        return details;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public List<String> getAttributeValues() {
        if( this.attributeValues == null ) {
            this.attributeValues = new ArrayList<>(1);
        }

        return attributeValues;
    }

    public void setAttributeValues(List<String> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public void addAttributeValue(String attributeValue) {
        getAttributeValues().add(attributeValue);
    }

    @Override
    public String toString() {
        return "ErrorDetails{" +
                "reason='" + reason + '\'' +
                ", attributeName='" + attributeName + '\'' +
                ", attributeValues=" + attributeValues +
                '}';
    }
}