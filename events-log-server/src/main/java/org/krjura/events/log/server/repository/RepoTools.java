package org.krjura.events.log.server.repository;

import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import java.util.Objects;

public class RepoTools {

    private RepoTools() {
        // util
    }

    public static Integer extractId(RowSet<Row> rows) {
        Objects.requireNonNull(rows);

        if(rows.rowCount() == 0 || rows.rowCount() > 1) {
            return null;
        }

        var iterator = rows.iterator();
        var row = iterator.next();

        return row.getInteger("id");
    }
}
