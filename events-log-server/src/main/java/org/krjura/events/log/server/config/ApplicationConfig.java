package org.krjura.events.log.server.config;

public class ApplicationConfig {

    private PostgreSqlConfig postgreSqlConfig;

    public PostgreSqlConfig getPostgreSqlConfig() {
        return postgreSqlConfig;
    }

    public void setPostgreSqlConfig(PostgreSqlConfig postgreSqlConfig) {
        this.postgreSqlConfig = postgreSqlConfig;
    }
}
