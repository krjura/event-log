package org.krjura.events.log.server.handlers.generic;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import org.krjura.events.log.server.enums.ServerConst;
import org.krjura.events.log.server.utils.JsonUtils;

import java.io.IOException;

import static java.util.Objects.requireNonNull;

public class JsonBodyHandler<T> implements Handler<RoutingContext> {

    private final Class<T> clazz;

    public JsonBodyHandler(Class<T> clazz) {
        this.clazz = requireNonNull(clazz);
    }

    public static <T> JsonBodyHandler<T> of(Class<T> clazz) {
        return new JsonBodyHandler<>(clazz);
    }

    @Override
    public void handle(RoutingContext context) {
        var body = context.getBodyAsString();

        if(body == null) {
            context.response().setStatusCode(400).end("body not defined");
            return;
        }

        try {
            T obj = JsonUtils.objectMapper().readValue(body, clazz);
            context.put(ServerConst.CONST_PROPS_OBJECT, obj);

            context.next();
        } catch (IOException e) {
            context.response().setStatusCode(400).end(e.getMessage());
        }
    }
}
