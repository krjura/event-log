package org.krjura.events.log.server.repository;

import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.Tuple;
import org.krjura.events.log.server.ex.ServerDetailedException;
import org.krjura.events.log.server.model.AppUserModel;
import org.krjura.events.log.server.utils.Either;
import org.krjura.events.log.server.utils.EitherType;

import java.util.concurrent.CompletableFuture;

import static java.util.Objects.requireNonNull;
import static org.krjura.events.log.server.repository.RepoTools.extractId;

public class AppUserRepository {

    public static final String QUERIES_SELECT_BY_ID = "SELECT id, email from app_user WHERE id = $1";
    public static final String QUERIES_SAVE = "INSERT INTO app_user (email) VALUES ($1) RETURNING id";

    private static final EitherType<AppUserModel> EITHER = new EitherType<>(AppUserModel.class);

    private final PostgresConnector connector;

    public AppUserRepository(PostgresConnector connector) {
        this.connector = requireNonNull(connector);
    }

    public CompletableFuture<Either<AppUserModel>> save(String email) {
        requireNonNull(email);

        var future = new CompletableFuture<Either<AppUserModel>>();

        connector
                .getClient()
                .preparedQuery(QUERIES_SAVE, Tuple.of(email), callback -> {
                    if(callback.succeeded()) {
                        var id = extractId(callback.result());

                        var user = new AppUserModel(id, email);
                        future.complete(EITHER.success(user));
                    } else {
                        future.complete(EITHER.error(callback.cause()));
                    }
        });

        return future;
    }

    public CompletableFuture<Either<AppUserModel>> selectAppUserById(Integer id) {
        requireNonNull(id);

        var future = new CompletableFuture<Either<AppUserModel>>();

        connector.getClient().preparedQuery(QUERIES_SELECT_BY_ID, Tuple.of(id), callback -> {
            if(callback.succeeded()) {
                future.complete(selectById(callback.result()));
            } else {
                future.complete(EITHER.error(callback.cause()));
            }
        });

        return future;
    }

    private Either<AppUserModel> selectById(RowSet<Row> rows) {
        if(rows.rowCount() > 1) {
            return EITHER.error(ServerDetailedException.of("events.db.nonUnique", "entity", "app_user"));
        } else if(rows.rowCount() == 0) {
            return EITHER.error(ServerDetailedException.of("events.db.notFound", "entity", "app_user"));
        }

        var iterator = rows.iterator();
        var row = iterator.next();

        return EITHER.success(mapRow(row));
    }

    private static AppUserModel mapRow(Row row) {
        return new AppUserModel(
                row.getInteger("id"),
                row.getString("email")
        );
    }
}
