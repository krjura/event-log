package org.krjura.events.log.server.handlers.events;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import org.krjura.events.log.server.enums.ServerConst;
import org.krjura.events.log.server.model.AppUserModel;
import org.krjura.events.log.server.model.EventModel;
import org.krjura.events.log.server.repository.AppUserRepository;
import org.krjura.events.log.server.repository.EventRepository;
import org.krjura.events.log.server.utils.Either;
import org.krjura.events.log.server.web.events.CreateEventRequest;

import java.time.ZonedDateTime;

import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onErrorResponse;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onJsonbResult;
import static org.krjura.events.log.server.web.events.EventResponseHelper.buildEventResponse;

public class CreateEventHandler implements Handler<RoutingContext> {

    private final AppUserRepository appUserRepo;

    private final EventRepository eventRepo;

    public CreateEventHandler(AppUserRepository appUserRepo, EventRepository eventRepo) {
        this.appUserRepo = Objects.requireNonNull(appUserRepo);
        this.eventRepo = Objects.requireNonNull(eventRepo);
    }

    public static CreateEventHandler of(AppUserRepository appUserRepository, EventRepository eventRepository) {
        return new CreateEventHandler(appUserRepository, eventRepository);
    }

    @Override
    public void handle(RoutingContext ctx) {

        var data = (CreateEventRequest) ctx.get(ServerConst.CONST_PROPS_OBJECT);

        appUserRepo
                .selectAppUserById(data.getUserId())
                .exceptionally(th -> Either.error(AppUserModel.class, th))
                .thenCompose(it -> onAppUserResponse(it, data))
                .thenAccept(it -> {
                    it.onSuccess(event -> onJsonbResult(ctx, buildEventResponse(event)));
                    it.onError(throwable -> onErrorResponse(ctx, throwable));
                });
    }

    public CompletableFuture<Either<EventModel>> onAppUserResponse(
            Either<AppUserModel> userResult, CreateEventRequest request) {

        if(userResult.isError()) {
            return CompletableFuture.completedFuture(Either.error(EventModel.class, userResult.getError()));
        }

        var user = userResult.getData();
        var now = ZonedDateTime.now();

        return eventRepo
                .save(EventModel.of(user.getUserId(), request.getSubject(), request.getDescription(), now, now));
    }
}
