package org.krjura.events.log.server.handlers.events;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import org.krjura.events.log.server.repository.EventRepository;
import org.krjura.events.log.server.utils.Either;
import org.krjura.events.log.server.utils.TypeRef;

import java.util.Objects;

import static org.krjura.events.log.server.handlers.ParamConst.PARAM_USER_ID;
import static org.krjura.events.log.server.handlers.utils.PagingUtils.extractPage;
import static org.krjura.events.log.server.handlers.utils.PagingUtils.extractSize;
import static org.krjura.events.log.server.handlers.utils.PathParamsUtils.paramAsInteger;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onErrorResponse;
import static org.krjura.events.log.server.handlers.utils.ResponseUtils.onJsonbResult;
import static org.krjura.events.log.server.web.events.EventResponseHelper.buildEventsResponse;

public class ListEventsHandler implements Handler<RoutingContext> {

    private final EventRepository eventsRepo;

    public ListEventsHandler(EventRepository eventsRepo) {
        this.eventsRepo = Objects.requireNonNull(eventsRepo);
    }

    public static ListEventsHandler of(EventRepository eventRepository) {
        return new ListEventsHandler(eventRepository);
    }

    @Override
    public void handle(RoutingContext ctx) {
        var page = extractPage(ctx, 0);
        var size = extractSize(ctx, 50);

        // use ParamCheckHandler to ensure param exists
        var userId = paramAsInteger(ctx, PARAM_USER_ID);

        eventsRepo
                .list(userId, page, size)
                .exceptionally(throwable -> Either.error(new TypeRef<>(), throwable))
                .thenAccept(it -> {
                    it.onSuccess(events -> onJsonbResult(ctx, buildEventsResponse(events)));
                    it.onError(throwable -> onErrorResponse(ctx, throwable));
                });
    }
}
