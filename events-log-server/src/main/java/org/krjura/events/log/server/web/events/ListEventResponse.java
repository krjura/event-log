package org.krjura.events.log.server.web.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListEventResponse {

    private List<EventResponse> events;

    @JsonCreator
    public ListEventResponse(
            @JsonProperty("events") List<EventResponse> events) {

        this.events = Objects.requireNonNull(events);
    }

    public List<EventResponse> getEvents() {
        return events;
    }
}
