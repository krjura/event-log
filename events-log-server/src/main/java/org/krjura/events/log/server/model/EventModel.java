package org.krjura.events.log.server.model;

import java.time.ZonedDateTime;
import java.util.Objects;

public class EventModel {

    private final Integer id;

    private final Integer userId;

    private final String subject;

    private final String description;

    private final ZonedDateTime createdAt;

    private final ZonedDateTime modifiedAt;

    public EventModel(
            final Integer id,
            final Integer userId,
            final String subject,
            final String description,
            final ZonedDateTime createdAt,
            final ZonedDateTime modifiedAt) {

        this.id = id;
        this.userId = Objects.requireNonNull(userId);
        this.subject = Objects.requireNonNull(subject);
        this.description = Objects.requireNonNull(description);
        this.createdAt = Objects.requireNonNull(createdAt);
        this.modifiedAt = Objects.requireNonNull(modifiedAt);
    }

    public static EventModel of(
            final Integer userId,
            final String subject,
            final String description,
            final ZonedDateTime createdAt,
            final ZonedDateTime modifiedAt) {

        return new EventModel(null, userId, subject, description, createdAt, modifiedAt);
    }

    public static EventModel of(
            final Integer id,
            final Integer userId,
            final String subject,
            final String description,
            final ZonedDateTime createdAt,
            final ZonedDateTime modifiedAt) {

        return new EventModel(id, userId, subject, description, createdAt, modifiedAt);
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public ZonedDateTime getModifiedAt() {
        return modifiedAt;
    }
}