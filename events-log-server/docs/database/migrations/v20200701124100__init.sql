CREATE TABLE app_user (
    id SERIAL NOT NULL,

    email VARCHAR(255) NOT NULL,

    CONSTRAINT pk_app_user PRIMARY KEY (id)
);

ALTER TABLE app_user ADD CONSTRAINT unq_app_user_email UNIQUE (email);

CREATE TABLE event (
    id SERIAL NOT NULL,
    user_id INTEGER NOT NULL,
    subject VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    modified_at TIMESTAMP WITH TIME ZONE NOT NULL,

    CONSTRAINT pk_event PRIMARY KEY (id)
);

ALTER TABLE event ADD CONSTRAINT fk_event_user_id FOREIGN KEY (user_id) REFERENCES app_user (id);

CREATE TABLE event_record (
    id SERIAL NOT NULL,
    user_id INTEGER NOT NULL,
    event_id INTEGER NOT NULL,
    description VARCHAR(255) NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    modified_at TIMESTAMP WITH TIME ZONE NOT NULL,

    CONSTRAINT pk_event_record PRIMARY KEY (id)
);

ALTER TABLE event_record ADD CONSTRAINT fk_event_record_user_id FOREIGN KEY (user_id) REFERENCES app_user (id);
ALTER TABLE event_record ADD CONSTRAINT fk_event_record_event_id FOREIGN KEY (event_id) REFERENCES event (id);